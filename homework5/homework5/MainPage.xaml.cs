﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Diagnostics;
using Xamarin.Forms.Maps;
using Xamarin.Essentials;
namespace homework5
//SOURCES: EXAMPLE ON COUGAR COURSES, ONLINE RESOURCES, AND HELP FROM CLASSMATES
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
          InitializeComponent();
            InitMap();
            PlaceAMarker();
        }
        public void InitMap()
        {
            Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.034086, -117.291940), Distance.FromMiles(1)));
        }
        private void Button_Clicked(object sender, EventArgs e)
        {
            Map.MapType = MapType.Satellite;
            B1.BackgroundColor = Color.Blue;
            B2.BackgroundColor = Color.Black;
            B3.BackgroundColor = Color.Black;
        }

        private void Button_Clicked_1(object sender, EventArgs e)
        {
            Map.MapType = MapType.Hybrid;
            B1.BackgroundColor = Color.Black;
            B2.BackgroundColor = Color.Blue;
            B3.BackgroundColor = Color.Black;
        }

        private void Button_Clicked_2(object sender, EventArgs e)
        {
            Map.MapType = MapType.Street;
            B1.BackgroundColor = Color.Black;
            B2.BackgroundColor = Color.Black;
            B3.BackgroundColor = Color.Blue;
        }

        private void PlaceAMarker()
        {
            var SwamisPos = new Position(33.034086, -117.291940);
            var BeaconsPos = new Position(33.064938, -117.305258);
            var PontoPos = new Position(33.085116, -117.312201);
            var StatePos = new Position(33.113369, -117.324235);
            var OceansidePos = new Position(33.193948, -117.384955);
            var Swamis = new Pin
            {
                Type = PinType.Place,
                Position = SwamisPos,
                Label = "Swamis",
                Address = "1341 1st St, Encinitas, CA 92024"
            };

            var Beacons = new Pin
            {
                Type = PinType.Place,
                Position = BeaconsPos,
                Label = "Beacons",
                Address = "948 Neptune Ave, Encinitas, CA 92024"
            };
            var Ponto = new Pin
            {
                Type = PinType.Place,
                Position = PontoPos,
                Label = "Ponto",
                Address = "2100 Carlsbad Blvd, Encinitas, CA 92024"
            };
            var State = new Pin
            {
                Type = PinType.Place,
                Position = StatePos,
                Label = "State Street",
                Address = "5703 Carlsbad Blvd, Carlsbad, CA 92008"
            };
            var Oceanside = new Pin
            {
                Type = PinType.Place,
                Position = OceansidePos,
                Label = "Oceanside Pier",
                Address = "The Strand N, Oceanside, CA 92054"
            };

            Map.Pins.Add(Swamis);
            Map.Pins.Add(Ponto);
            Map.Pins.Add(Beacons);
            Map.Pins.Add(State);
            Map.Pins.Add(Oceanside);
        }


        private void Picker_SelectedIndexChanged(object sender, EventArgs e)
        {
            picker.Title = "Picker";
            picker.TitleColor = Color.Red;
            var item = (Picker)sender;
            int selected = item.SelectedIndex;
            if(selected == 0)
            {
                Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.034086, -117.291940), Distance.FromMiles(1)));
            }
            else if(selected == 1)
            {
                Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.064938, -117.305258), Distance.FromMiles(1)));
            }
            else if(selected == 2)
            {
                Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.085116, -117.312201), Distance.FromMiles(1)));
            }
            else if(selected == 3)
            {
                Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.113369, -117.324235), Distance.FromMiles(1)));
            }
            else if(selected == 4)
            {
                Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.193948, -117.384955), Distance.FromMiles(1)));
            }
            picker.Title = "Picker";
            picker.TitleColor = Color.Red;
        }

        private void ContentPage_Appearing(object sender, EventArgs e)
        {
            B3.BackgroundColor = Color.Blue;
        }
    }
}
